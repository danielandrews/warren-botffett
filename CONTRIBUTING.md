# How to contribute to Warren Botffett

### Follow the GitFlow branching style <br>

Create a feature or bugfix branch off of the develop branch and then create a
pull request when you are done!

