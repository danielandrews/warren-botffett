"use strict";

//external libraries

// This loads the environment variables from the .env file
require('dotenv-extended').load();

const restify = require('restify');
const builder = require('botbuilder');
const _ = require('lodash');

//local libraries/files

var compoundInterestCalculator = require("./compoundInterest.js");
var projectedSavings = require("./projectedSavings.js");

//Load Definitions for financial acronyms
const acronyms = require('./acronyms.json');
//Load Definitions for credit cards
const creditCards = require('./creditcard.json');

// Setup Restify Server
var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function() {
    console.log('%s listening to %s', server.name, server.url);
});

//=========================================================
// Bot Setup
//=========================================================

// Create connector and listen for messages
var connector = new builder.ChatConnector({
    appId: process.env.MICROSOFT_APP_ID,
    appPassword: process.env.MICROSOFT_APP_PASSWORD
});

// Listen for messages
server.post('/api/messages', connector.listen());

// Create the bot
var bot = new builder.UniversalBot(connector);

// You can provide your own model by specifing the 'LUIS_MODEL_URL' environment variable
// This Url can be obtained by uploading or creating your model from the LUIS portal: https://www.luis.ai/
const LUIS_URL = process.env.LUIS_MODEL_URL;
var recognizer = new builder.LuisRecognizer(LUIS_URL);
bot.recognizer(recognizer);

//=========================================================
// Bot Dialogs
//=========================================================

bot.dialog('retrieve definition', [
    function(session, args, next) {
        // try extracting entities
        var acronymEntity = builder.EntityRecognizer.findEntity(args.intent.entities, 'acronym');

        if (acronymEntity) {
            // acronym entity detected, continue to next step
            next({ response: acronymEntity.entity });
        } else {
            // no entities detected, ask user for a financial term
            builder.Prompts.text(session, 'I don\'t quite understand. Can I help you with anything else?');
        }
    },
    function(session, results) {
        var acronym = results.response;

        //Lookup the search term in our acronyms dictionary
        var entity = acronym.toUpperCase();
        session.send(acronyms[entity].Name);
        session.send(acronyms[entity].Definition);

        session.send('Can I help you with anything else?');
        session.endDialog();
    }
]).triggerAction({
    matches: 'retrieve definition',
    onInterrupted: function(session) {
        builder.Prompts.text(session, 'Can I help you with anything else?');
    }
});

bot.dialog('openaccount ', [
    function(session, args, next) {
        session.send('Based on your income, it would be more beneficial to open an RRSP. Open one here: https://www.tdcanadatrust.com/products-services/investing/rsps/rsp-index.jsp');
        session.send('Can I help you with anything else?');
        session.endDialog();
    }
]).triggerAction({
    matches: 'openaccount ',
    onInterrupted: function(session) {
        session.send('Based on your income, it would be more beneficial to open an RRSP. Open one here: https://www.tdcanadatrust.com/products-services/investing/rsps/rsp-index.jsp');
        session.send('Can I help you with anything else?');
        session.endDialog();
    }
});

bot.dialog('calculate savings', [
    function(session, args, next) {
        builder.Prompts.number(session, "What is your initial investment?");
    },
    function(session, args, next) {
        session.userData.initialSavings = args.response;
        builder.Prompts.number(session, "How much money do you expect to invest per month?");
    },
    function(session, args, next) {
        session.userData.contribution = args.response;
        builder.Prompts.number(session, "And finally, across how many years?");
    },
    function(session, results) {
        session.userData.timePeriod = results.response;

        session.send('This is what your projected savings are!');

        var initialSavings = session.userData.initialSavings;
        var contribution = session.userData.contribution;
        var timePeriod = session.userData.timePeriod;

        var currentYear = new Date().getFullYear();

        var xAxisInYears = _.range(currentYear, (currentYear + timePeriod + 1));
        
        var yAxisInSavings = _.range(0, timePeriod).map(function(year){
            return compoundInterestCalculator.calculateBalanceForCurrentYear(initialSavings, contribution, year);
        });

        projectedSavings.generateSavingsGraph(xAxisInYears, yAxisInSavings, function(msg){
            session.send(msg.url);
        })

        session.endDialog();
    }
]).triggerAction({
    matches: 'calculate savings',
    onInterrupted: function(session) {
        session.send('Would you like to continue?');
    }
});

bot.dialog('Help', function(session) {
    session.endDialog('Hi! Try asking me things like \'What is a TFSA?\', \'What is the best credit card?\' or \'I want to save more money!\'');
}).triggerAction({
    matches: 'Help'
});

bot.dialog('creditcard', [
    function(session, args, next) {
        builder.Prompts.choice(session, 'Are you willing to pay an annual fee to earn rewards on your card?', ['yes', 'no']);
    },
    function(session, results, next) {
        session.userData.acceptsAnnualFee = results.response;
        builder.Prompts.choice(session, 'What is the number one benefit you want from your Credit Card?', ['aeroplan', 'points', 'cashback']);

    },
    function(session, results) {
        session.userData.benefit = results.response;

        var acceptsFees = session.userData.acceptsAnnualFee["entity"];
        var benefitSelected = session.userData.benefit["entity"];

        var card;

        // 1. Check if user wants annual fees
        if (acceptsFees === "yes") {
            // Accepts fees
            for (var key in creditCards) {
                if (creditCards[key].annual_fee !== 0 && creditCards[key].benefits === benefitSelected) {
                    card = creditCards[key];
                    break;
                }
            }
        } else {
            // Rejects fees
            for (var key in creditCards) {
                if (creditCards[key].annual_fee === 0 && creditCards[key].benefits === benefitSelected) {
                    card = creditCards[key];
                    break;
                } else {
                    // TODO: Fall back credit card if no options match
                    card = creditCards["8"];
                }
            }
        }

        session.send("Here you are: " + card.name);
        session.send(card.img_url);
        session.send(card.apply_link);
        
        // End
        session.endDialog();
    }

]).triggerAction({
    matches: 'creditcard',
    onInterrupted: function(session) {
        session.send('Would you like to continue?');
    }
});