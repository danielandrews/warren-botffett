# Warren Botffett

# A simple financial advisor chat bot using Bot Builder by Microsoft

## Table of Contents
- [Setup](#setup)
- [Available Scripts](#available-scripts)
     - [npm start](#npm-start)

## Setup

Please install the Bot Framework Channel Emulator from Microsoft at: <br>

https://emulator.botframework.com/

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Run the Bot Framework Channel Emulator.
Open [http://localhost:3978/api/messages](http://localhost:3978/api/messages) in the emulator.