/*
Generate graph of projected savings using Plotly

x_years : array of years
y_savings: array of monetary saving corresponding to x_years
callback: callback to be executed

return : void
*/

// This loads the environment variables from the .env file
require('dotenv-extended').load();

const PLOTLY_USERID = process.env.PLOTLY_USERID;
const PLOTLY_API_KEY = process.env.PLOTLY_API_KEY;
const plotly = require('plotly')(PLOTLY_USERID, PLOTLY_API_KEY);

exports.generateSavingsGraph = function (x_years, y_savings, callback) {

       //Setup the data for the graph
        var data = [
            {
                x: x_years,
                y: y_savings,
                type: "line"
            }
        ];

        //Call the plotly API to generate the graph
        var graphOptions = { filename: "date-axes", fileopt: "overwrite" };
        plotly.plot(data, graphOptions, function(err, msg) {
            callback(msg);
        });
}