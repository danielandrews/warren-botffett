/*
Calculate balances per year based on compound interest formula with monthly contributions

Compound Interest for Principal : A = P(1+r/n)^(nt)

A = the future value of the investment/loan, including interest
P = the principal investment amount (the initial deposit or loan amount)
r = the annual interest rate (decimal)
n = the number of times that interest is compounded per year
t = the number of years the money is invested or borrowed for

Future Value of Series : A = PMT × (((1 + r/n)^nt - 1) / (r/n))

A = the future value of the investment/loan, including interest
P = the principal investment amount (the initial deposit or loan amount)
PMT = the monthly payment
r = the annual interest rate (decimal)
n = the number of times that interest is compounded per year AND additional payment frequency
t = the number of years the money is invested or borrowed for

Total = Compound Interest for Principal + Future Value of Series
*/

exports.calculateBalanceForCurrentYear = function (initialSavings, contribution, year) {

    //Assume an average expected rate of return of 5%
    const AVERAGE_EXPECTED_RETURNS_PERCENT = 5 / 100;
    //Compound monthly
    const BASE_COMPOUND_PERIOD = 12;
    //Compound interest rate
    const COMPOUND_RATE = 1 + (AVERAGE_EXPECTED_RETURNS_PERCENT / BASE_COMPOUND_PERIOD);
    var compoundPeriods = BASE_COMPOUND_PERIOD * year;
    var compoundMultiplier = Math.pow(COMPOUND_RATE, compoundPeriods);
    
    var compoundInterestForPrincipal = initialSavings * compoundMultiplier;
    var futureValueOfSeries = contribution * ((compoundMultiplier - 1)/(AVERAGE_EXPECTED_RETURNS_PERCENT / BASE_COMPOUND_PERIOD));
    var balanceForCurrentYear = (compoundInterestForPrincipal + futureValueOfSeries).toFixed(2);

    return balanceForCurrentYear;
}